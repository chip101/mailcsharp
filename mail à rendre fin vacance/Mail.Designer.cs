﻿namespace mail_à_rendre_fin_vacance
{
    partial class Mail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mail));
            this.btnAjouterCopieCache = new System.Windows.Forms.Button();
            this.btnAjouterPJ = new System.Windows.Forms.Button();
            this.btnAjouterCopie = new System.Windows.Forms.Button();
            this.btnAjouterDestinataire = new System.Windows.Forms.Button();
            this.cbxServeur = new System.Windows.Forms.ComboBox();
            this.tbxPJ = new System.Windows.Forms.TextBox();
            this.tbxMessage = new System.Windows.Forms.TextBox();
            this.tbxDestinataire = new System.Windows.Forms.TextBox();
            this.tbxCopieCache = new System.Windows.Forms.TextBox();
            this.tbxCopie = new System.Windows.Forms.TextBox();
            this.tbxExpediteur = new System.Windows.Forms.TextBox();
            this.tbxObjet = new System.Windows.Forms.TextBox();
            this.btnEnvoyer = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.titre = new System.Windows.Forms.Label();
            this.cbxDestinataire = new System.Windows.Forms.ComboBox();
            this.cbxCopieCachée = new System.Windows.Forms.ComboBox();
            this.cbxCopie = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAjouterCopieCache
            // 
            this.btnAjouterCopieCache.Location = new System.Drawing.Point(1118, 209);
            this.btnAjouterCopieCache.Name = "btnAjouterCopieCache";
            this.btnAjouterCopieCache.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterCopieCache.TabIndex = 50;
            this.btnAjouterCopieCache.Text = "Ajouter";
            this.btnAjouterCopieCache.UseVisualStyleBackColor = true;
            this.btnAjouterCopieCache.Click += new System.EventHandler(this.btnAjouterCopieCache_Click);
            // 
            // btnAjouterPJ
            // 
            this.btnAjouterPJ.Location = new System.Drawing.Point(949, 306);
            this.btnAjouterPJ.Name = "btnAjouterPJ";
            this.btnAjouterPJ.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterPJ.TabIndex = 49;
            this.btnAjouterPJ.Text = "Ajouter";
            this.btnAjouterPJ.UseVisualStyleBackColor = true;
            this.btnAjouterPJ.Click += new System.EventHandler(this.btnAjouterPJ_Click);
            // 
            // btnAjouterCopie
            // 
            this.btnAjouterCopie.Location = new System.Drawing.Point(1118, 249);
            this.btnAjouterCopie.Name = "btnAjouterCopie";
            this.btnAjouterCopie.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterCopie.TabIndex = 48;
            this.btnAjouterCopie.Text = "Ajouter";
            this.btnAjouterCopie.UseVisualStyleBackColor = true;
            this.btnAjouterCopie.Click += new System.EventHandler(this.btnAjouterCopie_Click);
            // 
            // btnAjouterDestinataire
            // 
            this.btnAjouterDestinataire.Location = new System.Drawing.Point(1118, 164);
            this.btnAjouterDestinataire.Name = "btnAjouterDestinataire";
            this.btnAjouterDestinataire.Size = new System.Drawing.Size(75, 23);
            this.btnAjouterDestinataire.TabIndex = 47;
            this.btnAjouterDestinataire.Text = "Ajouter";
            this.btnAjouterDestinataire.UseVisualStyleBackColor = true;
            this.btnAjouterDestinataire.Click += new System.EventHandler(this.btnAjouterDestinataire_Click);
            // 
            // cbxServeur
            // 
            this.cbxServeur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxServeur.FormattingEnabled = true;
            this.cbxServeur.Location = new System.Drawing.Point(1024, 86);
            this.cbxServeur.Name = "cbxServeur";
            this.cbxServeur.Size = new System.Drawing.Size(205, 21);
            this.cbxServeur.TabIndex = 46;
            // 
            // tbxPJ
            // 
            this.tbxPJ.Enabled = false;
            this.tbxPJ.Location = new System.Drawing.Point(322, 309);
            this.tbxPJ.Name = "tbxPJ";
            this.tbxPJ.Size = new System.Drawing.Size(611, 20);
            this.tbxPJ.TabIndex = 45;
            // 
            // tbxMessage
            // 
            this.tbxMessage.Location = new System.Drawing.Point(322, 432);
            this.tbxMessage.Multiline = true;
            this.tbxMessage.Name = "tbxMessage";
            this.tbxMessage.Size = new System.Drawing.Size(631, 179);
            this.tbxMessage.TabIndex = 44;
            this.tbxMessage.TextChanged += new System.EventHandler(this.tbxMessage_TextChanged);
            // 
            // tbxDestinataire
            // 
            this.tbxDestinataire.Enabled = false;
            this.tbxDestinataire.Location = new System.Drawing.Point(322, 164);
            this.tbxDestinataire.Name = "tbxDestinataire";
            this.tbxDestinataire.Size = new System.Drawing.Size(611, 20);
            this.tbxDestinataire.TabIndex = 43;
            // 
            // tbxCopieCache
            // 
            this.tbxCopieCache.Enabled = false;
            this.tbxCopieCache.Location = new System.Drawing.Point(322, 209);
            this.tbxCopieCache.Name = "tbxCopieCache";
            this.tbxCopieCache.Size = new System.Drawing.Size(611, 20);
            this.tbxCopieCache.TabIndex = 42;
            // 
            // tbxCopie
            // 
            this.tbxCopie.Enabled = false;
            this.tbxCopie.Location = new System.Drawing.Point(322, 249);
            this.tbxCopie.Name = "tbxCopie";
            this.tbxCopie.Size = new System.Drawing.Size(611, 20);
            this.tbxCopie.TabIndex = 41;
            // 
            // tbxExpediteur
            // 
            this.tbxExpediteur.Location = new System.Drawing.Point(148, 86);
            this.tbxExpediteur.Name = "tbxExpediteur";
            this.tbxExpediteur.Size = new System.Drawing.Size(253, 20);
            this.tbxExpediteur.TabIndex = 40;
            // 
            // tbxObjet
            // 
            this.tbxObjet.Location = new System.Drawing.Point(322, 388);
            this.tbxObjet.Name = "tbxObjet";
            this.tbxObjet.Size = new System.Drawing.Size(611, 20);
            this.tbxObjet.TabIndex = 39;
            // 
            // btnEnvoyer
            // 
            this.btnEnvoyer.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEnvoyer.Location = new System.Drawing.Point(1072, 640);
            this.btnEnvoyer.Name = "btnEnvoyer";
            this.btnEnvoyer.Size = new System.Drawing.Size(160, 84);
            this.btnEnvoyer.TabIndex = 38;
            this.btnEnvoyer.Text = "Envoyer";
            this.btnEnvoyer.UseVisualStyleBackColor = false;
            this.btnEnvoyer.Click += new System.EventHandler(this.btnEnvoyer_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(38, 640);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(157, 84);
            this.btnAnnuler.TabIndex = 37;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(196, 432);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 22);
            this.label1.TabIndex = 36;
            this.label1.Text = "Message :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(166, 307);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 22);
            this.label8.TabIndex = 35;
            this.label8.Text = "Pièce jointes :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(225, 386);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 22);
            this.label7.TabIndex = 34;
            this.label7.Text = "Objet :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(24, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 22);
            this.label6.TabIndex = 33;
            this.label6.Text = "Expediteur :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(158, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 22);
            this.label5.TabIndex = 32;
            this.label5.Text = "Copie cachée :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(172, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 22);
            this.label4.TabIndex = 31;
            this.label4.Text = "Destinataire :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(225, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 22);
            this.label3.TabIndex = 30;
            this.label3.Text = "Copie :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(870, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 22);
            this.label2.TabIndex = 29;
            this.label2.Text = "Serveur SMTP :";
            // 
            // titre
            // 
            this.titre.AutoSize = true;
            this.titre.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titre.ForeColor = System.Drawing.SystemColors.ControlText;
            this.titre.Location = new System.Drawing.Point(462, 9);
            this.titre.Name = "titre";
            this.titre.Size = new System.Drawing.Size(328, 46);
            this.titre.TabIndex = 28;
            this.titre.Text = "Aperture Express";
            // 
            // cbxDestinataire
            // 
            this.cbxDestinataire.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDestinataire.FormattingEnabled = true;
            this.cbxDestinataire.Location = new System.Drawing.Point(949, 164);
            this.cbxDestinataire.Name = "cbxDestinataire";
            this.cbxDestinataire.Size = new System.Drawing.Size(148, 21);
            this.cbxDestinataire.TabIndex = 51;
            this.cbxDestinataire.SelectedIndexChanged += new System.EventHandler(this.cbxDestinataire_SelectedIndexChanged);
            // 
            // cbxCopieCachée
            // 
            this.cbxCopieCachée.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCopieCachée.FormattingEnabled = true;
            this.cbxCopieCachée.Location = new System.Drawing.Point(949, 209);
            this.cbxCopieCachée.Name = "cbxCopieCachée";
            this.cbxCopieCachée.Size = new System.Drawing.Size(148, 21);
            this.cbxCopieCachée.TabIndex = 52;
            // 
            // cbxCopie
            // 
            this.cbxCopie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxCopie.FormattingEnabled = true;
            this.cbxCopie.Location = new System.Drawing.Point(949, 248);
            this.cbxCopie.Name = "cbxCopie";
            this.cbxCopie.Size = new System.Drawing.Size(148, 21);
            this.cbxCopie.TabIndex = 53;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1209, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(39, 23);
            this.button1.TabIndex = 54;
            this.button1.Text = "supp";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1209, 209);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(39, 23);
            this.button2.TabIndex = 55;
            this.button2.Text = "supp";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1209, 249);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(39, 23);
            this.button3.TabIndex = 56;
            this.button3.Text = "supp";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1030, 306);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(39, 23);
            this.button4.TabIndex = 57;
            this.button4.Text = "supp";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Mail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1268, 752);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbxCopie);
            this.Controls.Add(this.cbxCopieCachée);
            this.Controls.Add(this.cbxDestinataire);
            this.Controls.Add(this.btnAjouterCopieCache);
            this.Controls.Add(this.btnAjouterPJ);
            this.Controls.Add(this.btnAjouterCopie);
            this.Controls.Add(this.btnAjouterDestinataire);
            this.Controls.Add(this.cbxServeur);
            this.Controls.Add(this.tbxPJ);
            this.Controls.Add(this.tbxMessage);
            this.Controls.Add(this.tbxDestinataire);
            this.Controls.Add(this.tbxCopieCache);
            this.Controls.Add(this.tbxCopie);
            this.Controls.Add(this.tbxExpediteur);
            this.Controls.Add(this.tbxObjet);
            this.Controls.Add(this.btnEnvoyer);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.titre);
            this.Name = "Mail";
            this.Text = "Mail";
            this.Load += new System.EventHandler(this.Mail_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAjouterCopieCache;
        private System.Windows.Forms.Button btnAjouterPJ;
        private System.Windows.Forms.Button btnAjouterCopie;
        private System.Windows.Forms.Button btnAjouterDestinataire;
        private System.Windows.Forms.ComboBox cbxServeur;
        private System.Windows.Forms.TextBox tbxPJ;
        private System.Windows.Forms.TextBox tbxMessage;
        private System.Windows.Forms.TextBox tbxDestinataire;
        private System.Windows.Forms.TextBox tbxCopieCache;
        private System.Windows.Forms.TextBox tbxCopie;
        private System.Windows.Forms.TextBox tbxExpediteur;
        private System.Windows.Forms.TextBox tbxObjet;
        private System.Windows.Forms.Button btnEnvoyer;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label titre;
        private System.Windows.Forms.ComboBox cbxDestinataire;
        private System.Windows.Forms.ComboBox cbxCopieCachée;
        private System.Windows.Forms.ComboBox cbxCopie;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}