﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace mail_à_rendre_fin_vacance
{
    public partial class Serveur : Form
    {
        Boolean ajouterServeur = false;
        public Serveur()
        {
            InitializeComponent();
        }

        private void Serveur_Load(object sender, EventArgs e)
        {
            if (File.Exists(@"..\\..\\..\\serveur.txt"))
            {

            }
            else
            {
                FileStream serveur = File.Create(@"..\\..\\..\\serveur.txt");
                serveur.Close();
                serveur.Dispose();

            }
            
            tbxAdresseServeur.Enabled = false;
           



            btnEnregistrer.Visible = false;
            btnAnnuler.Visible = false;

            string[] tabAdresseServeur = lbxItemContact.GetTabAdresseServeur();
            for (int i = 0; i < tabAdresseServeur.Length; i++)
            {
                lbxServeur.Items.Add(tabAdresseServeur[i].ToString());
            }

        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            ajouterServeur = true;
            /// LORS DE L AJOUT ON DEGRISE ET CHANGE LA VISIBILITE DE CERTAINS BOUTONS///
            tbxAdresseServeur.Enabled = true;
            


            btnModifier.Visible = false;
            btnEnregistrer.Visible = true;
            btnAnnuler.Visible = true;
            btnSupprimer.Visible = false;
            btnAjouter.Visible = false;
            /////////// ON REINITIALISE LES TBX//////

            tbxAdresseServeur.Text = "";
            

            ///////// ON GRISE LA LISTBOX//////
            lbxServeur.Enabled = false;
           
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            ///// ANNULATION ON CHANGE DE BOUTONS/////
            btnModifier.Enabled = true;
            btnModifier.Visible = true;
            btnEnregistrer.Visible = false;
            btnAnnuler.Visible = false;
            btnAjouter.Visible = true;
            btnSupprimer.Visible = true;

            ////// ANNULATION ON VIDE ET GRISE LES TBX//////
            tbxAdresseServeur.Enabled = false;
            

            tbxAdresseServeur.Text = "";
            
            ajouterServeur = false;
            lbxServeur.Enabled = true;
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            btnModifier.Enabled = false;
            btnEnregistrer.Visible = true;
            btnAnnuler.Visible = true;
            btnSupprimer.Visible = false;
            btnAjouter.Visible = false;

            tbxAdresseServeur.Enabled = true;
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Êtes vous sûres de vouloir supprimer ce serveur SMTP ?", "Avertissement suppression", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                ProcedureTxt.SupprimerServeur(tbxAdresseServeur.Text);
                Application.Restart();

            }
            else if (dialogResult == DialogResult.No)
            {

            }
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            if (ajouterServeur == true)
            {
                if (tbxAdresseServeur.Text != "" )
                {
                    ProcedureTxt.AjouterServeur(tbxAdresseServeur.Text);
                    Application.Restart();
                }
                else
                {
                    MessageBox.Show("Veuillez Remplir le champ correctement , le champ ne doit pas contenir d'espace");
                }
            }
            if ( ajouterServeur == false)
            {

            }
        }

        private void lbxServeur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxServeur.SelectedIndex != -1)
            {
                try
                {
                    int Index = lbxServeur.SelectedIndex;

                    string AdresseServeur = lbxServeur.Items[Index].ToString();

                    tbxAdresseServeur.Text = AdresseServeur;
                }
                catch (Exception ex4)
                {
                    MessageBox.Show(ex4.Message);
                }
            }
            
        }
    }
}
