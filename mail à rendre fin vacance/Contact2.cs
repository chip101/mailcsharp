﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
namespace mail_à_rendre_fin_vacance
{
    public partial class Contact2 : Form
    {
        public static Boolean ajouterContact = false;
        public Contact2()
        {
            InitializeComponent();
        }

        public static void RemplirLbx()
        {


            int i = 0;
            string[] tabNomMailContact2 = lbxItemContact.GetTabNomPrenomContact();

            ListBox lbxContact = new ListBox();




            while (i < tabNomMailContact2.Length)
            {
                //MessageBox.Show(tabNomMailContact2[i], "tabContact au rang :" + i);
                i++;
            }

            //lbxContact.Items.AddRange(tabNomMailContact2.ToArray());
            //ArrayList Tbl = new ArrayList(); foreach (object item in lbxContact.Items) { Tbl.Add(item.ToString()); }

            //foreach (string item in Tbl)
            //{
            //    MessageBox.Show(item.ToString(), "Test");
            //}
            i = 0;
            lbxContact.ResetText();
            lbxContact.Items.Clear();
            // lbxContact.DataSource = tabNomMailContact2.ToArray();
            for (i = 0; i < tabNomMailContact2.Length; i++)
            {

                lbxContact.Items.Add(tabNomMailContact2[i].ToString());

            }






            i = 0;

        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Êtes vous sûres de vouloir supprimer ce contact ?", "Avertissement suppression", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                ProcedureTxt.SupprimerContact(tbxNom.Text, tbxPrenom.Text, tbxTel.Text, tbxMail.Text);
                Application.Restart();
            }
            else if (dialogResult == DialogResult.No)
            {

            }


        }

        private void Contact2_Load(object sender, EventArgs e)      /// EN APPELLANT UNE PROCEDURE POUR REMPLIR LBX --> ERREUR MAIS DIRECTEMENT DANS LE LOAD MARCHE?
        {
            if (File.Exists(@"..\\..\\..\\contact.txt"))
            {

            }
            else
            {
                FileStream contact = File.Create(@"..\\..\\..\\contact.txt");   // SI LE FICHIER CONTACT TXT N EXISTE PAS ON LE CREEE
                contact.Close();
                contact.Dispose();

            }
            tbxMail.Enabled = false;
            tbxTel.Enabled = false;
            tbxNom.Enabled = false;
            tbxPrenom.Enabled = false;



            btnEnregistrer.Visible = false;
            btnAnnuler.Visible = false;

            // RemplirLbx

            string[] tabNomMailContact2 = lbxItemContact.GetTabNomPrenomContact();
            for (int i = 0; i < tabNomMailContact2.Length; i++)
            {
                lbxContact.Items.Add(tabNomMailContact2[i].ToString());
            }

        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            //// POUR MODIFIER ON A LES TBX DEGRISE////
            tbxPrenom.Enabled = true;
            tbxTel.Enabled = true;
            tbxNom.Enabled = true;
            tbxMail.Enabled = true;


            //// QUAND ON MODIFIE ON A ACCES A DES BOUTONS DIFFERENTS////
            btnModifier.Enabled = false;
            btnEnregistrer.Visible = true;
            btnAnnuler.Visible = true;
            btnSupprimer.Visible = false;
            btnAjouter.Visible = false;



        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            ///// ANNULATION ON CHANGE DE BOUTONS/////
            btnModifier.Enabled = true;
            btnModifier.Visible = true;
            btnEnregistrer.Visible = false;
            btnAnnuler.Visible = false;
            btnAjouter.Visible = true;
            btnSupprimer.Visible = true;

            ////// ANNULATION ON VIDE ET GRISE LES TBX//////
            tbxNom.Enabled = false;
            tbxPrenom.Enabled = false;
            tbxTel.Enabled = false;
            tbxMail.Enabled = false;

            tbxMail.Text = "";
            tbxTel.Text = "";
            tbxPrenom.Text = "";
            tbxNom.Text = "";

            ajouterContact = false;
            lbxContact.Enabled = true ;
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            /// LORS DE L AJOUT ON DEGRISE ET CHANGE LA VISIBILITE DE CERTAINS BOUTONS///
            tbxPrenom.Enabled = true;
            tbxMail.Enabled = true;
            tbxNom.Enabled = true;
            tbxTel.Enabled = true;


            btnModifier.Visible = false;
            btnEnregistrer.Visible = true;
            btnAnnuler.Visible = true;
            btnSupprimer.Visible = false;
            btnAjouter.Visible = false;
            /////////// ON REINITIALISE LES TBX//////

            tbxPrenom.Text = "";
            tbxMail.Text = "";
            tbxNom.Text = "";
            tbxTel.Text = "";

            ///////// ON GRISE LA LISTBOX//////
            lbxContact.Enabled = false;
            ajouterContact = true;
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            if (ajouterContact == true)
            {
                if (tbxTel.Text != "" && tbxMail.Text != "" && tbxNom.Text != "" && tbxPrenom.Text != "")
                {
                    ProcedureTxt.AjouterContact(tbxNom.Text, tbxPrenom.Text, tbxTel.Text, tbxMail.Text);


                    
                    
                }
                else
                {
                    MessageBox.Show("Veuillez Remplir tous les champs correctement , un champ ne doit pas contenir d'espace");
                }
            }
            if (ajouterContact == false)
            {
                if (tbxTel.Text != "" && tbxMail.Text != "" && tbxNom.Text != "" && tbxPrenom.Text != "")
                {
                    string OldTel, OldNom, OldPrenom, OldMail;
                    int Index = lbxContact.SelectedIndex;
                    string NomPrenom = lbxContact.Items[Index].ToString();


                    String[] tabNomPrenom = NomPrenom.Split(' ');               // POUR MODIFIER ON VA SUPPRIMER L ANCIEN CONTACT ET AJOUTER LE NOUVEAU
                                                                                // POUR IDENTIFIER L ANCIEN CONTACT ON UTILISE L INDEX DE LA LBX 

                    string[] TempoTab = ProcedureTab.GetTempotabContactForNP(tabNomPrenom[1], tabNomPrenom[0]);
                    if (TempoTab == null)
                    {
                        return;
                    }
                    OldTel = TempoTab[2];
                    OldMail = TempoTab[3];
                    OldPrenom = tabNomPrenom[0];
                    OldNom = tabNomPrenom[1];           /// On recherche tous les champs du contact modifier intact
                                                        /// Puis on va passer en parametre les nouveaux champs et anciens
                                                        /// derriere dans la procedure on va en fait ajouter(les nouveaux champs) et supprimer (les anciens)
                    ProcedureTxt.SupprimerContact(OldNom, OldPrenom, OldTel, OldMail);
                    //MessageBox.Show("Old contact supp");

                    ProcedureTxt.AjouterContact(tbxNom.Text, tbxPrenom.Text, tbxTel.Text, tbxMail.Text);
                   // MessageBox.Show("New contact add");
                    
                }
                else
                {
                    MessageBox.Show("Veuillez Remplir tous les champs correctement , un champ ne doit pas contenir d'espace");
                }
               
            }

            
            Application.Restart();

        }

        private void lbxContact_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lbxContact.SelectedIndex != -1)
            {
                int Index = lbxContact.SelectedIndex;

                string NomPrenom = lbxContact.Items[Index].ToString();


                String[] tabNomPrenom = NomPrenom.Split(' ');

                string[] TempoTab = ProcedureTab.GetTempotabContactForNP(tabNomPrenom[1], tabNomPrenom[0]);        /// ICI ON REMPLIT LES LBX EN PRENANT L INFO DE L INDEX OU LE USER A CLIQUE
                if (TempoTab == null)                                                                               /// DE L INDEX ON A LE NOM PRENOM ET DE LA ON VA OBTENIR LE MAIL ET TEL VIA
                {                                                                                                   /// UNE PROCEDURE , ENSUITE ON AFFICHE LE TOUT
                    return;
                }
                tbxTel.Text = TempoTab[2];
                tbxMail.Text = TempoTab[3];
                tbxPrenom.Text = tabNomPrenom[0];
                tbxNom.Text = tabNomPrenom[1];
            }
            



        }

        
    }
}
