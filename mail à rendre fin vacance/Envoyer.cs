﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Windows.Forms;


namespace mail_à_rendre_fin_vacance
{
    class Envoyer
    {
        public static string finalMessage = "";

        public static void FinalEnvoyer(string serveur , string expediteur , string objet, string message, string destinataire, int c, string [] TabDestinaire) // int c servira a determiner le cas d un ajout normal / cache / copie 
        {
            
            SmtpClient clientSmtp = new SmtpClient(serveur);
            MessageBox.Show(" Voici le serveur SMTP " + serveur);
            MailMessage monMessage = new MailMessage();

            string premessage = message + "Ce message est groupé pour : ";
            if ( c== 0)
            {
                for (int i = 0; i < TabDestinaire.Length; i++)          /// cas ou c= 0 donc ajouter destinataire en serie
                {

                    finalMessage = finalMessage + " " + TabDestinaire[i];
                }
                
            }
            monMessage.Body = premessage + finalMessage;

            monMessage.Subject = objet;

            MailAddress Destinataire = new MailAddress(destinataire);
            monMessage.To.Add(Destinataire);

            MailAddress Expediteur = new MailAddress(expediteur);
            monMessage.From = Expediteur;

            try
            {
                clientSmtp.Send(monMessage);
                MessageBox.Show("Le message a été envoyé");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Echec : " + ex.Message);
            }
        }
    }
}
