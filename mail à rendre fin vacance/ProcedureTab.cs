﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace mail_à_rendre_fin_vacance
{
    class ProcedureTab
    {
        public static string[] tabContact;
        public static string[] TempotabContact;
        public static string ligne;
        public static int a = 0, c = 0;

        
        public static string[] GetTabContact()
        {
            
            if (File.Exists(@"..\\..\\..\\contact.txt"))
            {
                StreamReader SR = new StreamReader(@"..\\..\\..\\contact.txt");
                int b = File.ReadAllLines(@"..\\..\\..\\contact.txt").Count();
                tabContact = new string[b * 4];
                a = 0; c = 0;
                while ((ligne = SR.ReadLine()) != null)
                {
                    TempotabContact = ligne.Split(';');                 // ici on a un tabContact indexe de 0 à n contenant toutes les infos de contact
                    int c = 0;
                    while (c < TempotabContact.Length)
                    {
                        tabContact[a] = TempotabContact[c];
                        a++;
                        c ++;
                       
                        
                        
                    }
                }
                SR.Close();
                SR.Dispose();

                //int i = 0;
                //while (i < tabContact.Length)
                //{
                //    MessageBox.Show(tabContact[i], "tabContact au rang :" + i);           // POUR VERIFIER CONTENU TAB
                //    i++;
                //}

                return tabContact;
            }
            else
            {
                MessageBox.Show("Un probleme est survenu dans la lecture du fichier contact");
                return null;
            }
        }
        public static string [] GetTempotabContactForNP(string Nom,string Prenom)
        {
            Boolean trouve = false;
           
            StreamReader SR = new StreamReader(@"..\\..\\..\\contact.txt");

            while (trouve == false) 
            {
                while ((ligne = SR.ReadLine()) != null)
                {
                    TempotabContact = ligne.Split(';');
                    //MessageBox.Show(TempotabContact[0], "tab au rang 0");
                    //MessageBox.Show(TempotabContact[1], "tab au rang 1");
                    if (TempotabContact[0] == Nom && TempotabContact[1] == Prenom)          // ICI ON RECHERCHE UN CONTACT A PARTIR DE SON NOM PRENOM POUR ENSUITE AFFICHE LES AUTRES INFOS DANS LES TXTBOX
                    {
                        
                        trouve = true;
                        SR.Close();
                        SR.Dispose();
                        return  TempotabContact;
                    }


                }
                if (trouve == false)
                {
                    SR.Close();
                    SR.Dispose();
                    MessageBox.Show("Le contact n a pas été trouvé");
                    return null;
                }

            }



            return null; 
        }
        public static string [] GetTabDestinataire(string AllDestinataires)
        {
            int d = CompterOccurstring(AllDestinataires);
            string[] TabDestinataire = new string[d];
            TabDestinataire = AllDestinataires.Split(';');
            int i = 0;
            while (i < TabDestinataire.Length)
            {
               // MessageBox.Show(TabDestinataire[i], "tabDestinataire au rang :" + i);           // POUR VERIFIER CONTENU TAB
                i++;
            }
            return TabDestinataire;
            
            
        }
        public static int CompterOccurstring(string AllDestinataire)    // on compte nombre destinataire pour declarer notre nouveau tab
        {
            char occur = ';';
            int c = 0;
            for (int i =0; i < AllDestinataire.Length; i++)
            {
                if(AllDestinataire[i] == occur)
                {
                    c++;
                }

            }
            return c;
        }
    }

    class lbxItemContact
    {
        
        public static string[] tabNomPrenomContact;
        public static  string[] tabContact = ProcedureTab.GetTabContact();

        public static string[] GetTabNomPrenomContact()
        {
            int j=0,i = 0;
            while (i< tabContact.Length)
            {
                i++;
            }
            //MessageBox.Show(i.ToString(), "Voici i");
            tabNomPrenomContact = new string[i/4];
            i = 0;
            while (  j < tabContact.Length)
            {
                tabNomPrenomContact[i] = tabContact[j+1] + " " + tabContact[j];
               
                                                          /// ici on a un tab avec Nom Prenom Nom Prenom...
                i ++;
                j = j + 4;
                

            }
            return tabNomPrenomContact;
        }
        public static string[] GetTabAdresseServeur()
        {
            string ligne;
            int b = File.ReadAllLines(@"..\\..\\..\\serveur.txt").Count();

            string[] TabAdresseServeur = new string[b];

            StreamReader serveurSR = new StreamReader(@"..\\..\\..\\serveur.txt");

            int i = 0;
            while (!serveurSR.EndOfStream)                  //// RENVOIE UN TABLEAU DE TOUTES LES ADRESSES SERVEUR
            {
                ligne = serveurSR.ReadLine();
                TabAdresseServeur[i] = ligne;
                i++;

            }
            serveurSR.Close();
            serveurSR.Dispose();
            return TabAdresseServeur;

        }
        
    }
}
