﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;

namespace mail_à_rendre_fin_vacance
{
    public partial class Mail : Form
    {
        public Boolean Myst = false;
        public Mail()
        {
            InitializeComponent();
        }

        private void Mail_Load(object sender, EventArgs e)
        {
            string[] tabAdresse = lbxItemContact.GetTabAdresseServeur();
            for (int i =0; i< tabAdresse.Length; i++)
            {
                cbxServeur.Items.Add(tabAdresse[i]);
            }
            string[] tabContact = ProcedureTab.GetTabContact();
            for (int i = 0; 3+i*4 < tabContact.Length; i++)
            {
                cbxDestinataire.Items.Add(tabContact[3+4*i]);
                cbxCopieCachée.Items.Add(tabContact[3 + 4 * i]);
                cbxCopie.Items.Add(tabContact[3 + 4 * i]);
            }



        }

        private void cbxDestinataire_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tbxMessage_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnEnvoyer_Click(object sender, EventArgs e)
        {
            

            if (Myst == false)
            {
                Image myimage = new Bitmap(@"..\\..\\a.png");
                this.BackgroundImage = myimage;
                Myst = true;
            }
            try
            {
                
                if (tbxDestinataire.Text != "" && tbxExpediteur.Text != "" && tbxObjet.Text !="" && tbxMessage.Text != "" && cbxServeur.SelectedIndex != -1)
                {
                    string [] TabDestinataire = ProcedureTab.GetTabDestinataire(tbxDestinataire.Text);
                    for (int i = 0; i < TabDestinataire.Length; i++)
                    {
                        Envoyer.FinalEnvoyer(cbxServeur.SelectedItem.ToString(), tbxExpediteur.Text, tbxObjet.Text, tbxMessage.Text, TabDestinataire[i],  0, TabDestinataire);
                    }
                        
                }
                else
                {
                    MessageBox.Show(" Veuillez rentrer les champs de maniere correctes (minimum 1 Destinaitaire , 1 Expediteur , 1 serveur , 1 Objet , 1Message)");
                }
            }
            catch (Exception ex5)
            {
                MessageBox.Show(ex5.Message);
            }



        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            if (Myst == true)
            {
                Image myimage2 = new Bitmap(@"..\\..\\e.jpg");
                this.BackgroundImage = myimage2;
                Myst = false;
            }
            tbxCopie.Text = "";
            tbxCopieCache.Text = "";
            tbxDestinataire.Text = "";
            tbxExpediteur.Text = "";
            tbxMessage.Text = "";
            tbxObjet.Text = "";
            tbxPJ.Text = "";
            

           

        }

        private void btnAjouterDestinataire_Click(object sender, EventArgs e)
        {
            if (cbxDestinataire.SelectedIndex != -1)
            {
                if (tbxDestinataire.Text != "")
                {
                    
                    int index = cbxDestinataire.SelectedIndex;
                    tbxDestinataire.Text =tbxDestinataire.Text+";"+ cbxDestinataire.Items[index].ToString();
                }
                if (tbxDestinataire.Text == "")
                {
                    int index = cbxDestinataire.SelectedIndex;
                    tbxDestinataire.Text =  cbxDestinataire.Items[index].ToString();
                }
                
                    
                
            }
        }

        private void btnAjouterCopieCache_Click(object sender, EventArgs e)
        {
            if (cbxCopieCachée.SelectedIndex != -1)
            {
                if (tbxCopieCache.Text != "")
                {

                    int index = cbxCopieCachée.SelectedIndex;
                    tbxCopieCache.Text = tbxCopieCache.Text + ";" + cbxCopieCachée.Items[index].ToString();
                }
                if (tbxCopieCache.Text == "")
                {
                    int index = cbxCopieCachée.SelectedIndex;
                    tbxCopieCache.Text = cbxCopieCachée.Items[index].ToString();
                }
                


            }
        }

        private void btnAjouterCopie_Click(object sender, EventArgs e)
        {
            if (cbxCopie.SelectedIndex != -1)
            {
                if (tbxCopie.Text != "")
                {

                    int index = cbxCopie.SelectedIndex;
                    tbxCopie.Text = tbxCopie.Text + ";" + cbxCopie.Items[index].ToString();
                }
                if (tbxCopie.Text == "")
                {
                    int index = cbxCopie.SelectedIndex;
                    tbxCopie.Text = cbxCopie.Items[index].ToString();
                }



            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tbxDestinataire.Text = "";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            tbxCopieCache.Text = "";

        }

        private void button3_Click(object sender, EventArgs e)
        {
            tbxCopie.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tbxPJ.Text = "";
        }

        private void btnAjouterPJ_Click(object sender, EventArgs e)

        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                if (tbxPJ.Text == "")
                {
                    openFileDialog.Title = "Choississez un fichier";
                    openFileDialog.InitialDirectory = @"C:\Users\Desktop";
                    if (openFileDialog.ShowDialog(this) == DialogResult.OK)
                    {
                        tbxPJ.Text = openFileDialog.FileName.ToString();
                    }
                }
                else
                {
                    openFileDialog.Title = "Choississez un fichier";
                    openFileDialog.InitialDirectory = @"C:\Users\Desktop";
                    if (openFileDialog.ShowDialog(this) == DialogResult.OK)
                    {
                        tbxPJ.Text = tbxPJ.Text +";"+ openFileDialog.FileName.ToString();
                    }
                }
            }
               
        }
    } 
}
