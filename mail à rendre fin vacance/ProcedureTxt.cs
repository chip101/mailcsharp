﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace mail_à_rendre_fin_vacance
{
    class ProcedureTxt
    {
        public static void AjouterContact(string tbxNom, string tbxPrenom, string tbxTel,string tbxMail)
        {
            if (tbxNom != "" && tbxPrenom != "" && tbxMail != ""&& tbxTel != "")
            {
                int i, c = 0;
                string newMail = tbxMail;
                for (i = 0; i < newMail.Length; i++)
                {
                    if (newMail[i] == '@')                                  //// GERE SI PAS @ DANS LE MAIL////
                    {
                        c = c + 1;
                    }
                }
                if (c == 0)
                {
                    MessageBox.Show("Format Mail invalide (absence de @), veuillez vérifier votre saisie");
                    return;
                }
                else
                {



                    string ligne;
                    FileStream contact2 = File.Create(@"..\\..\\..\\contact2.txt");

                    contact2.Close();
                    contact2.Dispose();                                                         //// ON CREE UN CONTACT 2 OU ON VA ECRIRE LE CONTACT 1 + LA NOUVELLE LIGNE  ///

                    StreamReader contactSR = new StreamReader(@"..\\..\\..\\contact.txt");

                    StreamWriter contactSW = new StreamWriter(@"..\\..\\..\\contact2.txt");
                    while (!contactSR.EndOfStream)
                    {
                        ligne = contactSR.ReadLine();
                        contactSW.WriteLine(ligne);                                                         ///// ON RECOPIE CONTACT DANS CONTACT 2/////
                    }
                    contactSR.Close();
                    contactSR.Dispose();
                    contactSW.WriteLine(tbxNom + ";" + tbxPrenom + ";" + tbxTel + ";" + tbxMail);                                          //// ON RAJOUTE LE NOUVEAU CONTACT///
                    contactSW.Close();
                    contactSW.Dispose();
                    File.Replace((@"..\\..\\..\\contact2.txt"), @"..\\..\\..\\contact.txt", @"..\\..\\..\\OLDcontact.txt");
                    

                                                                                                                    /// LE FICHIER CONTACT 2 DEVIENT CONTACT////
                    

                    
                    File.Delete(@"..\\..\\..\\contact2.txt");                                                   //// ON SUPPRIME LE FICHIER CONTACT (ET OLDCONTACT CREE PAR LE REPLACE)////
                    File.Delete(@"..\\..\\..\\OLDcontact.txt");
                    MessageBox.Show("Le nouveau contact " + tbxNom + " a bien été créée");
                }



            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs");
            }
        }


        public static void SupprimerContact (string Nom, string Prenom, string Tel, string mail)
        {
            string[] tabSupp;
            if (Nom != "" && Prenom != "" && mail != ""&& Tel !="")
            {



                try
                {



                    string ligne;
                    FileStream contact2 = File.Create(@"..\\..\\..\\contact2.txt");

                    contact2.Close();
                    contact2.Dispose();                                                         //// ON CREE UN CONTACT 2 OU ON VA ECRIRE LE CONTACT 1 - LE CONTACT QUI A ETE SUPPRIME  ///

                    StreamReader contactSR = new StreamReader(@"..\\..\\..\\contact.txt");

                    StreamWriter contactSW = new StreamWriter(@"..\\..\\..\\contact2.txt");
                    while (!contactSR.EndOfStream)
                    {
                        ligne = contactSR.ReadLine();
                        tabSupp = ligne.Split(';');

                        if (tabSupp[0] != Nom && tabSupp[1] != Prenom && tabSupp[2] != Tel && tabSupp[3] != mail)
                        {
                            contactSW.WriteLine(ligne);

                        }
                        ///// ON RECOPIE CONTACT DANS CONTACT 2/////
                    }



                    contactSR.Close();
                    contactSR.Dispose();


                    contactSW.Close();
                    contactSW.Dispose();
                    //MessageBox.Show("TEST SUPPRESSION");


                    
                    File.Replace((@"..\\..\\..\\contact2.txt"), @"..\\..\\..\\contact.txt", @"..\\..\\..\\OLDcontact.txt");
                    File.Delete(@"..\\..\\..\\contact2.txt");
                    File.Delete(@"..\\..\\..\\OLDcontact.txt");        // ON SUPPRIME LES .TXT NE SERVANT PLUS A RIEN (COMME POUR AJOUTERCONTACT)
                    MessageBox.Show("L'ancien contact " + Nom + " a bien été supprimé.");
                }



                
                    
                
                catch (Exception ex4)
                {
                    MessageBox.Show(ex4.Message);
                }



            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs");
            }
        }
       
















        public static void AjouterServeur(string tbxAdresseServeur)
        {
            if (tbxAdresseServeur != "" )
            {
                
                try
                {



                    string ligne;
                    FileStream serveur2 = File.Create(@"..\\..\\..\\serveur2.txt");

                    serveur2.Close();
                    serveur2.Dispose();                                                         //// ON CREE UN SERVEUR 2 OU ON VA ECRIRE LE SERVEUR 1 + LA NOUVELLE LIGNE  ///

                    StreamReader serveurSR = new StreamReader(@"..\\..\\..\\serveur.txt");

                    StreamWriter serveurSW = new StreamWriter(@"..\\..\\..\\serveur2.txt");
                    while (!serveurSR.EndOfStream)
                    {
                        ligne = serveurSR.ReadLine();
                        serveurSW.WriteLine(ligne);                                                         ///// ON RECOPIE SERVEUR DANS SERVEUR 2/////
                    }
                    serveurSR.Close();
                    serveurSR.Dispose();

                    serveurSW.WriteLine(tbxAdresseServeur);                                         //// ON RAJOUTE LE NOUVEAU SERVEUR///

                    serveurSW.Close();
                    serveurSW.Dispose();
                    
                    //MessageBox.Show("Test avant replace serveur");
                    File.Replace((@"..\\..\\..\\serveur2.txt"), @"..\\..\\..\\serveur.txt", @"..\\..\\..\\OLDserveur.txt");


                    /// LE FICHIER SERVEUR 2 DEVIENT SERVEUR////

                    //MessageBox.Show("Test AVANT DELETE proctxt");

                    File.Delete(@"..\\..\\..\\OLDserveur.txt");                                                   //// ON SUPPRIME LE FICHIER SERVEUR////
                    File.Delete(@"..\\..\\..\\serveur2.txt");
                    MessageBox.Show("Le nouveau serveur " + tbxAdresseServeur + " a bien été créée");
                }
                catch (Exception ex2)
                {
                    MessageBox.Show(ex2.Message);
                }



            }
            else
            {
                MessageBox.Show("Vous devez remplir correctement le champ");
            }
        }









        public static void SupprimerServeur(string Serveur)
        {
            
            if (Serveur != "" )
            {



                try
                {



                    string ligne;
                    FileStream serveur2 = File.Create(@"..\\..\\..\\serveur2.txt");

                    serveur2.Close();
                    serveur2.Dispose();                                                         //// ON CREE UN SERVEUR 2 OU ON VA ECRIRE LE SERVEUR 1 - LE SERVEUR A ETE SUPPRIME  ///

                    StreamReader serveurSR = new StreamReader(@"..\\..\\..\\serveur.txt");

                    StreamWriter serveurSW = new StreamWriter(@"..\\..\\..\\serveur2.txt");
                    while (!serveurSR.EndOfStream)
                    {
                        ligne = serveurSR.ReadLine();
                        

                        if (ligne != Serveur)
                        {
                            serveurSW.WriteLine(ligne);

                        }
                        ///// ON RECOPIE SERVEUR DANS SERVEUR 2/////
                    }

                    

                    serveurSR.Close();
                    serveurSR.Dispose();


                    serveurSW.Close();
                    serveurSW.Dispose();

                    File.Replace((@"..\\..\\..\\serveur2.txt"), @"..\\..\\..\\serveur.txt", @"..\\..\\..\\OLDserveur.txt");
                    File.Delete(@"..\\..\\..\\serveur2.txt");
                    File.Delete(@"..\\..\\..\\OLDserveur.txt");





                    
                    //// ON SUPPRIME LE FICHIER SERVEUR////

                    MessageBox.Show("L'ancien serveur " + Serveur + " a bien été supprimé.");
                }
                catch (Exception ex3)
                {
                    MessageBox.Show("Une erreur est survenue lors de la suppression : "+ ex3.Message);
                }



            }
            else
            {
                MessageBox.Show("Vous devez remplir tous les champs");
            }
        }

    }
}
